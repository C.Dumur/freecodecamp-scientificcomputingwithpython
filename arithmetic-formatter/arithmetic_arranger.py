def arithmetic_arranger(problems, result=False):
    """
        problems est une liste de string contenant représentant les opérations à effectuer
        result est un booleen indiquant si l'on affiche les résultats
        Retourne une string contenant soit une erreur, soit le résultat formaté
    """
    # S'il y a plus de 5 problèmes, on retourne une erreur à l'utilisateur 
    if (len(problems) > 5):
        return "Error: Too many problems."

    result_solutions = []

    for p in problems:
        b,r = arrange_one_probleme(p, result)
        if not b:
            return r
        result_solutions.append(r)

    return formated_result(result_solutions)


def arrange_one_probleme(problem, result):
    """
        Traite un seul problème passé dans la liste problems de arithmetic_arranger
        Si le probleme est correctement formaté, retourne un tupple
            - un boolean à True,
            - un tableau de string représentant les lignes du problème formaté
        Sinon:
            - Un boolean à False,
            - L'erreur à afficher à l'utilisateur 
    """
    array_problem = problem.split(' ')
    operand1 = array_problem[0]
    operator = array_problem[1]
    operand2 = array_problem[2]

    if operator not in ['+', '-']:
        return (False, "Error: Operator must be '+' or '-'.")

    if len(operand1) > 4 or len(operand2) > 4:
        return (False, "Error: Numbers cannot be more than four digits.")

    if not operand1.isdecimal() or not operand2.isdecimal():
        return (False, "Error: Numbers must only contain digits.")

    res = []
    
    if (len(operand1) > len(operand2)):
        operand2 = operand2.rjust(len(operand1), ' ')
    else:
        operand1 = operand1.rjust(len(operand2), ' ')
    
    operand1 = '  ' + operand1
    operand2 = operator + ' ' + operand2
    res.append(operand1)
    res.append(operand2)

    max_len = len(operand1)
    separator = '-' * max_len
    res.append(separator)

    if result:
        if array_problem[1] == "+":
            result_line = int(array_problem[0]) + int(array_problem[2])
        else:
            result_line = int(array_problem[0]) - int(array_problem[2])
        
        result_line = str(result_line)
        result_line = result_line.rjust(max_len, ' ')
        res.append(result_line)

    return (True, res)

def formated_result(results):
    res = ""
    for i in range(0,len(results[0])):
        l = ""
        for j in range(0, len(results)):
            l = l + results[j][i] + '    '
        # On supprime les dernieres 4 espaces
        l = l[0:-4]
        res = res + l + '\n'
    # On supprime le dernier saut de ligne
    res = res[0:-1]
    return res