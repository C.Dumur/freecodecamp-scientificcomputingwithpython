# Solutions aux projets de la certification "Scientific Computing with Python" du freeCodeCamps

Vous trouverez sur ce repo, mes solutions aux 5 projets de la formation "Scientific Computing with Python" proposée par freeCodeCamp (https://www.freecodecamp.org/), a savoir :

- Arithmetic Formatter (https://www.freecodecamp.org/learn/scientific-computing-with-python/scientific-computing-with-python-projects/arithmetic-formatter)
- Time Calculator (https://www.freecodecamp.org/learn/scientific-computing-with-python/scientific-computing-with-python-projects/time-calculator)
- Budget App (https://www.freecodecamp.org/learn/scientific-computing-with-python/scientific-computing-with-python-projects/budget-app)
- Polygon Area Calculator (https://www.freecodecamp.org/learn/scientific-computing-with-python/scientific-computing-with-python-projects/polygon-area-calculator)
- Probability Calculator (https://www.freecodecamp.org/learn/scientific-computing-with-python/scientific-computing-with-python-projects/probability-calculator)

Les énoncés de ces projets, ainsi que les fichiers de tests ne sont pas présents sur ce repo, mais sont disponibles sur les dêpots repl.it mis à disposition par freeCodeCamp.