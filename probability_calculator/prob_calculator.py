import copy
import random

class Hat:
    def __init__(self, **kwargs):
        self.contents = []
        for k,v in kwargs.items():
            for i in range(v):
                self.contents.append(k)

    def draw(self, number):
        if number >= len(self.contents):
            return self.contents
        
        res = []

        while len(res) < number:
            # On détermine l'indice de la balle tiré
            i = random.randint(0,len(self.contents)-1)
            res.append(self.contents[i])
            self.contents.pop(i)

        return res

def experiment(hat, expected_balls, num_balls_drawn, num_experiments):
    success = 0
    for i in range(num_experiments):
        copy_hat = copy.deepcopy(hat)
        exp = copy_hat.draw(num_balls_drawn)
        if experiment_is_valid(list_to_dict(exp), expected_balls):
            success += 1
    return success / num_experiments

def list_to_dict(array):
    res = {}
    for e in array:
        if e in res:
            res[e] += 1
        else:
            res[e] = 1
    return res

def experiment_is_valid(experiment, expected):
    for k,v in expected.items():
        # Si on a tiré aucune balle de la couleur
        if k not in experiment:
            return False
        if v > experiment[k]:
            return False
    return True
