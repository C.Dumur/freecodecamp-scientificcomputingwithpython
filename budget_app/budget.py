class Category:
    def __init__(self, name):
        self.name = name
        self.ledger = []
    
    def __str__(self):
        # On centre la première ligne
        len_str = int((30 - len(self.name)) / 2)
        first_line = '*'*len_str + self.name + '*'*len_str
        if(len(first_line) != 30):
            first_line += '*'
        res = first_line + '\n'

        total = 0
        # On affiche les lignes amount
        for a in self.ledger:
            # Affichage de la partie description sur 23 caractères
            l = a['description']
            if len(l) <= 23:
                l = l.ljust(23, ' ')
            else:
                l = l[:23]

            # On affiche la somme sur 7 caractères
            amount = a['amount']
            total += amount
            amount = str(format(amount, '.2f'))
            amount = amount.rjust(7, ' ')

            res += l + amount + '\n'

        # On affiche la dernière ligne de toatal
        res += 'Total: ' + format(total, '.2f')
        return res


    def deposit(self, amount, description=""):
        self.ledger.append({'amount':amount, 'description':description})

    def get_balance(self):
        res = 0
        for d in self.ledger:
            res += d['amount']
        return res

    def withdraw(self, amount, description=""):
        if not self.check_funds(amount):
            return False
        self.ledger.append({'amount':amount*-1, 'description':description})
        return True

    def transfer(self, amount, budget):
        if not self.check_funds(amount):
            return False        
        self.withdraw(amount, "Transfer to " + budget.name)
        budget.deposit(amount, "Transfer from "+ self.name)
        return True

    def check_funds(self, amount):
        return self.get_balance() >= amount

    def total_withdraw(self):
        """
            Retourne le total des sommes dépensé dans la catégorie
        """
        res = 0
        for a in self.ledger:
            if a['amount'] < 0:
                res += a['amount'] * -1
        return res

def calc_percentage(categories):
    """
    Retourne un dict associant le nom de la catégorie au pourcentage de dépenses
    qu'elle représente
    """
    # On calcule les dépenses totale et stocke la répartition dans un dict
    withdraw_repartition = {}
    withdraw_total = 0

    for c in categories:
        withdraw_c = c.total_withdraw()
        withdraw_total += withdraw_c
        withdraw_repartition[c.name] = withdraw_c
    
    withdraw_repartition_percent = {}
    # On calcule le pourcentage pour chaque poste
    for (k,v) in withdraw_repartition.items():
        percent = (v * 100) / withdraw_total
        print(percent)
        withdraw_repartition_percent[k] = percent//10*10
    
    return withdraw_repartition_percent

def create_spend_chart(categories):
    repartition = calc_percentage(categories)

    # On calcule la taille maximale des libelles de légende
    len_max = 0
    for k,v in repartition.items():
        if len(k) > len_max:
            len_max = len(k)

    len_max += 1
    # On on calcule chaque colone comme une ligne de texte
    #La première colonne, est les centaines izaine des échelle
    lignes = []
    lignes.append((len_max * ' ') + (' ' * 10) + '1')
    #la ligne 2 représente les dizaines
    lignes.append(len_max * ' ' + ' 1234567890')
    #la ligne 3 représente les unités
    lignes.append(len_max * ' ' + '0' * 11)
    # Séparattion
    lignes.append(len_max * ' ' +'|' * 11)
    lignes.append(get_white_line(len_max))
    
    # On boucle ensuite sur le dico
    print(repartition)
    for k,v in repartition.items():
        count = int((v/10) + 1)
        print(count)
        line = 'o' * count
        line = line.ljust(11, ' ')
        lignes.append(k[::-1].rjust(len_max, ' ') + '-' + line)
        lignes.append(get_white_line(len_max))
        lignes.append(get_white_line(len_max))

    #lignes.append(get_white_line(len_max))
    res = line_to_column(lignes)
    res = res[:-1]
    res = 'Percentage spent by category\n' + res
    return res



def line_to_column(lines):

    n_lines = []
    for l in lines:
        n_lines.append(l[::-1])
   
    res = ''
    for i in range(0,len(n_lines[0])):
        line = ''
        for j in range(0,len(n_lines)):
            line += n_lines[j][i]
        res += line + '\n'
    return res

def get_white_line(len):
    return (' ' * (len)) +  '-' + (' '* 11)
