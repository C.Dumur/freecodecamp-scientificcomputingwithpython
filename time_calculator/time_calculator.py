def add_time(start, duration, day=""):
    total_minutes = time_since_new_day(start) + time_to_duration(duration)

    # On calcule l'heure du jour à couvertir et le nombre de jour écoulés
    count_days = total_minutes // (24 * 60)
    hours = total_minutes

    # On calcule l'heure
    hours_to_display = total_minutes_to_hour(hours)

    # On traite le cas du jour
    day_to_display = ""
    if(day != ""):
        day_to_display = ', ' + get_current_day(day, count_days)

    # On traite le nombre de jour passé
    latter_day = ""
    if count_days > 0:
        latter_day = " (" + str(count_days) + (" days later)")
        if count_days == 1:
            latter_day = " (next day)"

    return  hours_to_display + day_to_display + latter_day

def time_to_duration(time):
    """
    Retourne un heure au format HH:MM en nombre de minute
    """
    time_array = time.split(':')
    hours = time_array[0]
    minutes = time_array[1]

    return int(hours) * 60 + int(minutes)

def time_since_new_day(time):
    """
    Retourne le nombre de minutes écoulées depuis le début de la journée
    """
    time_array = time.split(" ")

    res = time_to_duration(time_array[0])

    # Si on est l'apres-midi, on ajoute les 13 * 60 heures déjà écoulées
    if(time_array[1].upper() == 'PM'):
        res += 12 * 60
    return res

def total_minutes_to_hour(minutes):
    array_hours = [
        ('12', 'AM'),('1', 'AM'),('2', 'AM'),('3', 'AM'),('4', 'AM'),('5', 'AM'),('6', 'AM'),('7', 'AM'),('8', 'AM'),
        ('9', 'AM'),('10', 'AM'),('11', 'AM'),('12', 'PM'),('1', 'PM'),('2', 'PM'),('3', 'PM'),('4', 'PM'),('5', 'PM'),
        ('6', 'PM'),('7', 'PM'),('8', 'PM'),('9', 'PM'),('10', 'PM'),('11', 'PM')
    ]
    hour = minutes // 60
    minute = minutes % 60
    hour_display = array_hours[hour%24]
    
    return hour_display[0] + ':' + str(minute).rjust(2,'0') + ' ' + hour_display[1]

def get_current_day(day, count_days):
    array_days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" ]

    i_day = array_days.index(day.lower())
    i_day = (i_day + count_days) % 7
    return array_days[i_day].capitalize()
